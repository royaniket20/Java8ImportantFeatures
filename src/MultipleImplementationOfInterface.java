import functionInterfaces.ParentInterfaceFunctional;

public class MultipleImplementationOfInterface {

	ParentInterfaceFunctional implementation1 = ()->System.out.println("This is implementation 1");
	ParentInterfaceFunctional implementation2 = ()->System.out.println("This is implementation 2");
	ParentInterfaceFunctional implementation3 = ()->System.out.println("This is implementation 3");

	public static void main(String[] args) {
		System.out.println("--Running many implementation of same method in a calss");
		MultipleImplementationOfInterface implementationOfInterface = new MultipleImplementationOfInterface();
		implementationOfInterface.implementation1.call();
		implementationOfInterface.implementation2.call();
		implementationOfInterface.implementation3.call();
		
	}
	
}
