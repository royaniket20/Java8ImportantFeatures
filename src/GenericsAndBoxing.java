
public class GenericsAndBoxing {

	public static void main(String[] args) {
		Integer I = 10;
		int i = I.intValue();//unboxing
		I = Integer.valueOf(i);//autoboxing
		//compiler do internally
		
	}
}
