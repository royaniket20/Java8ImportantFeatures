package functions;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class FunctionExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
Function<String, Integer> func = (str)->str.length();
String arr[] = {"ANIKET","AMIT","RIMI"};

for (int i = 0; i < arr.length; i++) {
	System.out.println("*****"+func.apply(arr[i]));
}


//get student grade

ArrayList<Strudent> stds = new ArrayList<Strudent>();
stds.add(new Strudent("Chinny", 56));
stds.add(new Strudent("Bunny", 85));
stds.add(new Strudent("Anny", 98));
stds.add(new Strudent("Munny", 50));
stds.add(new Strudent("Kanny", 30));

Function<Strudent, String> stdMark = student->{
	if(student.getMarks()>=80)
	{
		return "First Class";
	}
	else
	{
		return "Normal Student";
	}
};

stds.forEach(std->{
	System.out.println("Grade of Student : "+std.getName()+"   "+std.getMarks()+"   "+stdMark.apply(std));
});



//calculate total salary of all employees

ArrayList<Employee> emps = new ArrayList<Employee>();
emps.add(new Employee("Aniket", 584785.265));
emps.add(new Employee("Amit", 453435.265));
emps.add(new Employee("Urvashi", 3423.265));
emps.add(new Employee("Rimi", 35467.265));
emps.add(new Employee("Anirban", 3213.265));
emps.add(new Employee("Alibaba", 87978.265));
emps.add(new Employee("Sohini", 213123.265));

Function<List<Employee>, Double> totalSal = employees ->{
	double tot = 0;
	for(Employee emp:employees)
	{
		tot = tot + emp.getSalary();
	}
	return tot;
};

System.out.println("--Salary of total employees ----"+totalSal.apply(emps));


//salary increment whose salary is less than 50000 by 10000

Predicate<Employee> checkSalary = emp ->{
	return emp.getSalary()<=50000.00;
};

Function<Employee, Double> increaseSal = employee ->{
	double tot = 0;
	tot = employee.getSalary()+10000;
	return tot;
};

Consumer<List<Employee>> empPrinter = employeees->{
	employeees.forEach(System.out::println);
};

emps.forEach(k-> {
	if(checkSalary.test(k))
	{
	k.setSalary(increaseSal.apply(k));	
	}
});
empPrinter.accept(emps);

//then and compose difference
Function<Integer,Integer> f1 = i->i+i;
Function<Integer,Integer> f2 = i->i*i;
Function<Integer, Integer> ff1 = f1.andThen(f2);
Function<Integer, Integer> ff2 = f1.compose(f2);
System.out.println("---andthen----"+ff1.apply(2));
System.out.println("---compose----"+ff2.apply(2));






	}

}
