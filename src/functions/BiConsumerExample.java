package functions;

import java.util.function.BiConsumer;

public class BiConsumerExample {

	public static void main(String[] args) {
		BiConsumer<Integer, Integer> biPredicate = (x,y)->{
			System.out.println("ADDITION : "+x+y);
				
		};
		
		
		biPredicate.accept(6, 3);
	}
}
