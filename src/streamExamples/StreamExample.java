package streamExamples;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamExample {

	public static void main(String[] args) {
		
		ArrayList<Integer> list = new ArrayList<Integer>();
		list.add(10);
		list.add(11);
		list.add(11);
		list.add(11);
		list.add(12);
		list.add(14);
		list.add(15);
		list.add(13);
		
		
		System.out.println("Intstream sum() ans avarage() operayion");
		System.out.println(IntStream.of(5,4).average().getAsDouble());
		
		System.out.println("****distinct element******");
		System.out.println(list.stream().distinct().collect(Collectors.toList()));
		
		ArrayList<ArrayList<Integer>> listOfList = new ArrayList<>();
		listOfList.add(list);
		listOfList.add(list);
		listOfList.add(list);
		
		Stream<Integer> streamres1 = listOfList.stream().flatMap(k->k.stream());
		System.out.println("***flatmap example");
		streamres1.forEach(System.out::println);
		System.out.println("***flatmap example end");
		
		
		
		Iterator<Integer> itr = list.iterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
			//itr.remove(); // remove current element - but in multithreaded env it will show error as someone
			//else can modify the list
		}
		
		
		
		System.out.println("***peek()*** operation***");
		System.out.println("****"+list.stream().peek(k->System.out.println(k)).collect(Collectors.toList()));
		//findFirst
		Optional<Integer> resu1 = list.stream().findFirst();
		System.out.println(resu1.get());
		//-----
		System.out.println("***original****"+list);
		//get all even numbers
		Stream<Integer> stream = list.stream();
		
		Predicate<Integer> evenChecker = I->I%2==0;
		List<Integer> result = stream.filter(evenChecker).collect(Collectors.toList());
		
		System.out.println("***result****"+result);
		//each element need to be doubled
		
		Function<Integer, Integer> doubleMaker = I->I*2;
		 stream = list.stream();
		 result = stream.map(doubleMaker).collect(Collectors.toList());
			
			System.out.println("***result****"+result);
			
			
			ArrayList<String> listName = new ArrayList<String>();
			listName.add("Aniket");
			listName.add("Rimi");
			listName.add("Sriparna");
			listName.add("Krishna");
			
			System.out.println("***original****"+listName);
			//get all even numbers
			Stream<String> streamName = listName.stream();
			
			Function<String	,String> nameMaker = I->I.toUpperCase();
			long resultCount = streamName.map(nameMaker).count();
			
			System.out.println("***result****"+resultCount);
			
			Stream srotingData = list.stream();
			Stream sorted = srotingData.sorted();
			List<Integer> res1 = (List<Integer>) sorted.collect(Collectors.toList());
			System.out.println("----res1---"+res1);
			
			//now let's use comp-arator
			Comparator<Integer> comp = (i,j)->{
				if(i<j)
					return 1;
				else if(i==j)
					return 0;
				else
					return -1;
			};
			
			 srotingData = list.stream();
			 sorted = srotingData.sorted(comp);
			List<Integer> res2 = (List<Integer>) sorted.collect(Collectors.toList());
			System.out.println("----res2---"+res2);
			
			
			 srotingData = list.stream();
			
			//Integer min = (Integer) srotingData.min(comp).get();
			Integer max = (Integer) srotingData.max(comp).get();
			System.out.println("----Max---"+max);
			
			//using of for each
			srotingData = list.stream();
			srotingData.forEach(System.out::println);
			
			Integer[] arr =  list.stream().toArray(Integer[]::new);
		System.out.println(Arrays.toString(arr));
		
		//use of stream of method
		
		Stream<Integer> str = Stream.of(9,99,88,55,44,77);
		str.forEach(System.out::println);
		Double d[] = {88.5,58.236,48.2355,45.258};
		Stream<Double> std = Stream.of(d);
		std.forEach(System.out::println);
		
		
		
		
		
	}
}
