package streamExamples;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamExampleSpecialOne {

	public static void main(String[] args) {
		List<Integer> datas  = Arrays.asList(1,2,3,4,5,6,7,8,9,0);
		
		Map<String, List<Integer>> dat = datas.stream().collect(Collectors.groupingBy(k->{
			if(k%2==0)
				return "even";
			else
				return "odd";
		}));
		
		System.out.println(dat);
		
		
		  Stream.generate(Math::random).limit(10).forEach(System.out::println);
	}

}
