package functionInterfaces;

//as we are  explicitly say it is functional interface
@FunctionalInterface
public interface SumTwoNumberIntf {
public int sumTwoNumbers(int a , int b);

//public int someotherMethod(int a , int b); - will give error

public static void staticMethod()
{
	//THIS IS COMPLETELY ALLOWED
}

public default void defaultMethod()
{
	//THIS IS COMPLETELY ALLOWED
}


}
