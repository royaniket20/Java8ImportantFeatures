package functionInterfaces;

interface I5 {
	static void p1()
	{
		System.out.println("I1 default method");
	}
}

public class InterfaceStaticMethodCase implements I5 {

	//tHIS IS NOT OVERRIDING
	public static void p1() {
		System.out.println("NEW IMPLEMENTATION THIS");
		
	}
	
	@SuppressWarnings("static-access")
	public static void main(String[] args) {
		InterfaceStaticMethodCase casDefaultMethodCase = new InterfaceStaticMethodCase();
		casDefaultMethodCase.p1();
		p1();
		I5.p1();
		
	}

	
}
