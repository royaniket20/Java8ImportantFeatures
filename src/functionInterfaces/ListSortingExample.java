package functionInterfaces;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ListSortingExample {




public static void main(String[] args) {

	//sample list of integers
	List<Integer> integers = new ArrayList<Integer>(Arrays.asList(6,4,23,6,8,6,4,3,3,5,6,56,6,4,3,4,6,3,345,345,35,34,5,3));
	
	System.out.println("Unsorted list : "+integers);
	Collections.sort(integers); //internally use compareTo method
	
	//after sorting using the normal way 
	System.out.println("Sorted list : "+integers);
	
	 integers = new ArrayList<Integer>(Arrays.asList(6,4,23,6,8,6,4,3,3,5,6,56,6,4,3,4,6,3,345,345,35,34,5,3));
	 
	 Collections.sort(integers, new MyDecendingComparator());
	 
	//after sorting using the Custom way 
		System.out.println("Sorted list : "+integers);
	 
	
}


}

//customer comparotor
class MyDecendingComparator implements Comparator<Integer>{

	@Override
	public int compare(Integer o1, Integer o2) {
		/*
		 * if(o1>o2) return -1; else if(o1<o2) return 1; else return 0;
		 */
		
		//shorted way
		return (o1>o2)?  -1:(o1<o2) ? 1:  0;
		
	}


	
}
