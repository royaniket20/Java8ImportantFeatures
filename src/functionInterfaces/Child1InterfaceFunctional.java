package functionInterfaces;

@FunctionalInterface
public interface Child1InterfaceFunctional  extends ParentInterfaceFunctional{

	public void call();// as we are overiding same method
	
	//public int run(); - this wiill be error
}
