package functionInterfaces;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class EmployeeSortingExampleLamda {

	//A better lamda implementation;
	static Comparator<Employee> myComparator =  (o1,o2)-> {
		return (o1.getEmpId()>o2.getEmpId())?  -1:(o1.getEmpId()<o2.getEmpId()) ? 1:  0;
	};


public static void main(String[] args) {

List<Employee> employees = new ArrayList<Employee>();
	
	employees.add(new Employee(100, "aniket"));
	employees.add(new Employee(300, "Sriparna"));
	employees.add(new Employee(200, "chinny"));
	employees.add(new Employee(350, "bunny"));
	
	System.out.println("List of employees - Default implementation : "+employees);
	
	Collections.sort(employees,myComparator);
	
		System.out.println("Sorted Employee List : "+employees);
	 
	
}


}
