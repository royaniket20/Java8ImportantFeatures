package functionInterfaces;
/**
 * 
 * @author Aniket Roy
 *
 */
interface ABC{
	public void m1();
}
interface CDE{
	public void p1();
}
public class AnnoymVsLamdaThisKeyword  {

	int k = 100;
	int classVar = 90;
	public void m2()
	{
		ABC abc = new ABC() {
			
			int k = 200;
			@Override
			public void m1() {
			System.out.println("Current instance variable : "+this.k);
			System.out.println("Outer Instance variable : "+AnnoymVsLamdaThisKeyword.this.k);
				
			}
		};
		abc.m1();
	}
	
	public void p2() {
		int methodVar = 90;
		CDE cde = ()->{
			classVar = 100;
			//methodVar=100; - need to be effectively final
			int k = 700;//this is just a local varible for method p1
			System.out.println("Outer class variable : "+this.k);
			System.out.println("Local variable of p1 : "+k);
		};
		cde.p1();
		
	}
	public static void main(String[] args) throws CloneNotSupportedException {
		
		AnnoymVsLamdaThisKeyword annoymVsLamdaThisKeyword = new AnnoymVsLamdaThisKeyword();
		annoymVsLamdaThisKeyword.m2();
		annoymVsLamdaThisKeyword.p2();
	}
	
	
}
