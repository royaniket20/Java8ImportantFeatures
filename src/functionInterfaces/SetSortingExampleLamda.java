package functionInterfaces;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SetSortingExampleLamda {

	//A better lamda implementation;
	static Comparator<Integer> myComparator =  (o1,o2)-> {
		return (o1>o2)?  -1:(o1<o2) ? 1:  0;
	};


public static void main(String[] args) {

	//sample list of integers
	List<Integer> integers = new ArrayList<Integer>(Arrays.asList(6,4,23,6,8,6,4,3,3,5,6,56,6,4,3,4,6,3,345,345,35,34,5,3));
	
	System.out.println("Unsorted list : "+integers);
	Collections.sort(integers); //internally use compareTo method
	
	//after sorting using the normal way 
	System.out.println("Sorted list : "+integers);
	
	 integers = new ArrayList<Integer>(Arrays.asList(6,4,23,6,8,6,4,3,3,5,6,56,6,4,3,4,6,3,345,345,35,34,5,3));
	 
	 Collections.sort(integers, myComparator);
	 
	//after sorting using the Custom way 
		System.out.println("Sorted list : "+integers);
	 
	
}


}
