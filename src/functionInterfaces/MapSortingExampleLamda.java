package functionInterfaces;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

public class MapSortingExampleLamda {

	//A better lamda implementation;
	static Comparator<Integer> myComparator =  (o1,o2)-> {
		return (o1>o2)?  -1:(o1<o2) ? 1:  0;
	};


public static void main(String[] args) {

	//sample list of integers
	Map<Integer,String> namePari = new TreeMap<Integer, String>();//use default compareTO method to sort by keys
	
	namePari.put(100, "aniket");
	namePari.put(300, "Sriparna");
	namePari.put(200, "chinny");
	namePari.put(350, "bunny");
	
	System.out.println("Sorted Map by key - Default implementation : "+namePari);//default comapreTo methos is used
	

	
	namePari = new TreeMap<Integer, String>(myComparator);
	namePari.put(100, "aniket");
	namePari.put(300, "Sriparna");
	namePari.put(200, "chinny");
	namePari.put(350, "bunny");
	
	//after sorting using the Custom way 
		System.out.println("Sorted different map : "+namePari);
	 
	
}


}
