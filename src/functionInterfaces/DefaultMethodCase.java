package functionInterfaces;

interface I1 {
	default void p1()
	{
		System.out.println("I1 default method");
	}
}

interface I2 {
	default void p1()
	{
		System.out.println("I2 default method");
	}
}
public class DefaultMethodCase implements I1,I2 {

	@Override
	public  void p1() {
		System.out.println("OVERRIDING THIS");
		I1.super.p1();
	}
	
	public static void main(String[] args) {
		DefaultMethodCase casDefaultMethodCase = new DefaultMethodCase();
		casDefaultMethodCase.p1();
	}

	
}
