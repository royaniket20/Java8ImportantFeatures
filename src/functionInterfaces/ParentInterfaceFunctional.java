package functionInterfaces;

@FunctionalInterface
public interface ParentInterfaceFunctional {

	public void call();
}
