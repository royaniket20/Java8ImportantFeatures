package functionInterfaces;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class ListSortingExampleLamda {

	//A better lamda implementation;
	static Comparator<Integer> myComparator =  (o1,o2)-> {
		return (o1>o2)?  -1:(o1<o2) ? 1:  0;
	};


public static void main(String[] args) {

	//sample list of integers
	Set<Integer> integers = new TreeSet<Integer>(Arrays.asList(6,4,23,6,8,6,4,3,3,5,6,56,6,4,3,4,6,3,345,345,35,34,5,3));
	
	System.out.println("Sorted set : "+integers);//default comapreTo methos is used
	

	
	 integers = new TreeSet<Integer>(myComparator);
	 integers.addAll(Arrays.asList(6,4,23,6,8,6,4,3,3,5,6,56,6,4,3,4,6,3,345,345,35,34,5,3));
	 
	//after sorting using the Custom way 
		System.out.println("Sorted different set : "+integers);
	 
	
}


}
