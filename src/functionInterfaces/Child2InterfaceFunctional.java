package functionInterfaces;

@FunctionalInterface
public interface Child2InterfaceFunctional  extends ParentInterfaceFunctional{

	//this is still a function interface
	//public int run(); - this will be error as we already have inherited one function from parent
}
