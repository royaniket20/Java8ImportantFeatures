package functionInterfaces;

//Functiona interface - only one abstract method allowed
//@FunctionalInterface - as this is commented it may happen user may add additional methods and 
//turn it to normal interface
public interface HelloWorldIntf {

	public void printHelloWorld();
}
