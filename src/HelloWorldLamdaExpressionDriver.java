import functionInterfaces.HelloWorldIntf;
import functionInterfaces.SqureIntIntf;
import functionInterfaces.SumTwoNumberIntf;

public class HelloWorldLamdaExpressionDriver {

	//squre of number 
	SqureIntIntf squreIntIntf = x->x*x;
	
	
	//normal function
	public void printHeloWorld() {
		System.out.println("Hello Lamda !!!");
	}
	
	//lamda has no name . modifier , return type
	HelloWorldIntf intf=  ()->System.out.println("Hello Lamda");
	
	SumTwoNumberIntf numberIntf = (a,b)->{
		int result = a+b;
		System.out.println("Sum of "+ a+" & "+b+" is : "+result);
		return result;
	};
	


public static void main(String[] args) {
	
	
	HelloWorldLamdaExpressionDriver driver = new HelloWorldLamdaExpressionDriver();
	//calling void functions
	driver.intf.printHelloWorld();
	//calling functions which return something
	System.out.println("Final result : "+driver.numberIntf.sumTwoNumbers(5, 12));
	System.out.println("Squre result of 5: "+driver.squreIntIntf.squreInt(5));
		
	}


	
}
