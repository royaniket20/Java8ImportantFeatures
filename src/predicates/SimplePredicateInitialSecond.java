package predicates;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Predicate;

@FunctionalInterface
interface MyPredicateSecond
{
	public boolean test(Integer i);
}
public class SimplePredicateInitialSecond  {

	//Lets create normal method
	public boolean test (Integer I)
	
	{
		return I>100?true:false;
	}
	
	
	
	
	//A function interface
	MyPredicateSecond myPredicate = (I)->I>100;
	
	//Now create predicate
	Predicate<Integer> predicate =  (I)->I>100;
	Predicate<Integer> predicateSecond = (I)->I%2==0?true:false;
	Predicate<Collection<?>> collectionIsEmpty = (C)->C.isEmpty();
	public static void main(String[] args) {
		
		//Calling all of them 
		SimplePredicateInitialSecond initial = new SimplePredicateInitialSecond();
		System.out.println("Result from Normal Method : "+initial.test(500));
		System.out.println("Result from Functional Method : "+initial.myPredicate.test(300));
		System.out.println("Result from Predicate  Method : "+initial.predicate.test(55));
		System.out.println("Result from Predicate  Method NEGEATE : "+initial.predicate.negate().test(55));
		System.out.println("Result from Predicate  Method AND : "+initial.predicate.and(initial.predicateSecond).test(103));
		System.out.println("Result from Predicate  Method OR : "+initial.predicate.or(initial.predicateSecond).test(103));
		ArrayList<String> dataList = new ArrayList<String>();
		dataList.add("Aniket");
		System.out.println("Result for Collection Size check : "+initial.collectionIsEmpty.test(dataList));
		System.out.println("Result for Collection Size check Negeate : "+initial.collectionIsEmpty.negate().test(dataList));
	}
}
