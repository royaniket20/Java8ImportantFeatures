package predicates;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.function.Predicate;

class User{
	private String userName;
	private String passWord;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassWord() {
		return passWord;
	}
	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}
	
	
	
}
public class UserAuthUsingPredicate {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter user name : ");
		String userName = sc.next();
		System.out.println("Enter pass word : ");
		String passWord = sc.next();
		
		User user = new User();
		user.setUserName(userName);
		user.setPassWord(passWord);
		
		
		Predicate<User> userCheck = (u) ->{
			return u.getUserName().equals("admin") && u.getPassWord().equals("admin");
		};
		List<String> list = new ArrayList<String>();
		list.forEach(k->{
			
		});
	
		if(userCheck.test(user))
		{
			System.out.println("You are validated");
		}
		else
		{
			System.out.println("---Wrong user / pass");
		}
		
		
	}

}
