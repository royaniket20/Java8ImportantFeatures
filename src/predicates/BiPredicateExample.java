package predicates;

import java.util.function.BiPredicate;

public class BiPredicateExample {

	public static void main(String[] args) {
		BiPredicate<Integer, Integer> biPredicate = (x,y)->{
			if(x%2==0 && x>y)
				return true;
			else
				return false;
				
		};
		
		
		System.out.println("Check bipredicate : - "+biPredicate.test(6, 3));
	}
}
