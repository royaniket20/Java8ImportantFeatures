package predicates;

import java.util.function.IntPredicate;

public class PermitivePredicate {
public static void main(String[] args) {
	IntPredicate intPredicate = i->i%2==0;
	
	System.out.println("--in even 5--"+intPredicate.test(5));
}
}
