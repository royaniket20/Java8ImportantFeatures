package predicates;

import java.util.function.Predicate;

@FunctionalInterface
interface MyPredicate
{
	public boolean test(String i);
}
public class SimplePredicateInitial  {

	//Lets create normal method
	public boolean test (String I)
	
	{
		return I.length()>100?true:false;
	}
	
	//A function interface
	MyPredicate myPredicate = (I)->I.length()>100;
	
	
	//Now create predicate
	Predicate<String> predicate =  (I)->I.length()>100;
	public static void main(String[] args) {
		//Calling all of them 
		SimplePredicateInitial initial = new SimplePredicateInitial();
		System.out.println("Result from Normal Method : "+initial.test("ANIKET"));
		System.out.println("Result from Functional Method : "+initial.myPredicate.test("ANIKET"));
		System.out.println("Result from Predicate  Method : "+initial.predicate.test("ANIKET"));
	}
}
