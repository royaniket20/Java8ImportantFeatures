
public class RunMethodWithoutImplementKeywordJava {

	//no need to have extra class to implement runnable or callable
	Runnable runnable = ()->{
		//run method implementation
		System.out.println("--Do some thread task");
	};
	
	public static void main(String[] args) {
		RunMethodWithoutImplementKeywordJava myClassdObj = new RunMethodWithoutImplementKeywordJava();
		//creating thread
		Thread th1 = new Thread(myClassdObj.runnable);
		th1.start();
		//now code is much less
		
	}
}
