package PremitiveFunctioanInterfaces;

import java.util.function.Function;
import java.util.function.IntUnaryOperator;
import java.util.function.UnaryOperator;

public class UniaryOperatorExample {

	
	public static void main(String[] args) {
		
		//Normal function
		Function<Integer, Integer> f = i->i*i;
		
		//Same type of input 
		UnaryOperator<Integer> u = i->i*i;

		//To prevent unboxing and autoboxing
		
		IntUnaryOperator I = i->i*i;
		
		System.out.println("----Function "+f.apply(10));
		System.out.println("----UnaryOperator "+u.apply(10));
		System.out.println("----IntUnaryOperator "+I.applyAsInt(10));
		
	
	}
}
