package PremitiveFunctioanInterfaces;

@FunctionalInterface
interface Intf{
	
	public void m1();
}
public class MethodReferenceExample {

	public void m2()
	{
		System.out.println("****Calling from M2");
	}
	
	public static void m3()
	{
		System.out.println("****Calling from M3");
	}
	
	public static void main(String[] args) {
		
		
		//lamda
		Intf I1 = ()->{
			System.out.println("***Lamda implementation****");
		};
		MethodReferenceExample example = new MethodReferenceExample();
		
		Intf I2 = example::m2;
		
		Intf I3 = MethodReferenceExample::m3;
		
		System.out.println("****Calling-----");
		I1.m1();
		I2.m1();
		I3.m1();
		
		
		
	}
}
