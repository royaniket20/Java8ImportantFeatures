package PremitiveFunctioanInterfaces;

@FunctionalInterface
interface Intf2{
	
	public Example m1();
}

class Example{
	public Example()
	{
		System.out.println("****Example Constructor****");
	}
}
public class ConstructorReferenceExample {

	
	
	public static void main(String[] args) {
		
		
		//lamda
		Intf2 I1 = ()->{
			return new Example();
		};
		
		Intf2 I2 = Example::new;
		
	
		
		System.out.println("****Calling-----");
		I1.m1();
		I2.m1();
		
		
		
	}
}
