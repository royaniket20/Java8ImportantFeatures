package PremitiveFunctioanInterfaces;

import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.IntBinaryOperator;
import java.util.function.IntUnaryOperator;
import java.util.function.UnaryOperator;

public class BinaryOperatorExample {

	
	public static void main(String[] args) {
		
		//Normal function
		BiFunction<Integer,Integer, Integer> f = (i,j)->(i*j);
		
		//Same type of input 
		BinaryOperator<Integer> u =(i,j)->(i*j);

		//To prevent unboxing and autoboxing
		
		IntBinaryOperator I = (i,j)->(i*j);
		
		System.out.println("----BiFunction "+f.apply(10,20));
		System.out.println("----BinaryOperator "+u.apply(10,20));
		System.out.println("----IntBinaryOperator "+I.applyAsInt(10,20));
		
	
	}
}
