package PremitiveFunctioanInterfaces;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.Year;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class JodaDateTime {

	public static void main(String[] args) {
		LocalDate date = LocalDate.now();
		System.out.println("LocalDate----"+date);
	
		LocalTime time = LocalTime.now();
	    System.out.println("LocalTime----"+time);
	    
	    System.out.println("****Day----"+date.getDayOfMonth());
	    System.out.println("****Month----"+date.getMonthValue());
	    System.out.println("****Year----"+date.getYear());
	    
	    System.out.printf("%2d--%2d--%2d--%2d\n",time.getHour(),time.getMinute(),time.getSecond(),time.getNano());
	    
	    LocalDateTime dt =LocalDateTime.now();
	    
	    System.out.println("***Datetime****"+dt);
	    
	    System.out.println("****Day----"+dt.getDayOfMonth());
	    System.out.println("****Month----"+dt.getMonthValue());
	    System.out.println("****Year----"+dt.getYear());
	    
	    System.out.printf("%2d--%2d--%2d--%2d\n",dt.getHour(),dt.getMinute(),dt.getSecond(),dt.getNano());
	    
	    LocalDateTime dt2 = LocalDateTime.of(1992, 02, 02, 05, 45,55,48);
	    System.out.println("***Specific date time****"+dt2);
	    System.out.println("***6 months before****"+dt2.minusMonths(6));
	    
	    
	    Period p = Period.between(LocalDate.of(1992, 02, 02), LocalDate.now());
	    System.out.println(p.getDays()+"-----"+p.getYears());
	    
	    LocalDate nowDt = LocalDate.of(1956+69, 02, 02);
	    System.out.println(nowDt.toString());
	    
	    Year year = Year.now();
	    Year yy = Year.of(2020);
	    System.out.println(yy.getValue()+"---is leap---"+yy.isLeap());
	    
	    ZoneId id = ZoneId.systemDefault();
	    System.out.println(id);
	    id = ZoneId.of("America/Los_Angeles");
	    System.out.println(id);
	    System.out.println(ZonedDateTime.of(LocalDateTime.now()	, id));
	}
}
