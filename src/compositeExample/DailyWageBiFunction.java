package compositeExample;

import java.util.function.BiFunction;

public class DailyWageBiFunction {

	public static void main(String[] args) {
		BiFunction< Employee, TimeSheet	, Double> func = (emp,tsheet)->emp.getDialyWage()*tsheet.getWorkingDays();
		Employee employee = new Employee(100, "aniket", 458.23);
		TimeSheet timeSheet = new TimeSheet(100, 8745);
		System.out.println("result for emp : "+employee+" and timesheet "+timeSheet+"  :  "+func.apply(employee, timeSheet));
	}
}
