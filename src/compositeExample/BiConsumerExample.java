package compositeExample;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;

public class BiConsumerExample {
public static void main(String[] args) {
	  BiConsumer<AtomicInteger, Integer> setter = AtomicInteger::set;
      BiConsumer<AtomicInteger, Integer> adder = AtomicInteger::getAndAdd;
      BiConsumer<AtomicInteger, Integer> biConsumer = setter.andThen(adder);
      
      AtomicInteger ai = new AtomicInteger();
      biConsumer.accept(ai, 4);
      
      System.out.println(ai.get());
}
}
