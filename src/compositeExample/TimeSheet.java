package compositeExample;

public class TimeSheet {
private int eno;
private int workingDays;
@Override
public String toString() {
	return "TimeSheet [eno=" + eno + ", workingDays=" + workingDays + "]";
}
public TimeSheet(int eno, int workingDays) {
	super();
	this.eno = eno;
	this.workingDays = workingDays;
}
/**
 * @return the eno
 */
public int getEno() {
	return eno;
}
/**
 * @param eno the eno to set
 */
public void setEno(int eno) {
	this.eno = eno;
}
/**
 * @return the workingDays
 */
public int getWorkingDays() {
	return workingDays;
}
/**
 * @param workingDays the workingDays to set
 */
public void setWorkingDays(int workingDays) {
	this.workingDays = workingDays;
}

}
