package compositeExample;

public class Employee {
private int eno;
private String name;
private double dialyWage;
@Override
public String toString() {
	return "Employee [eno=" + eno + ", name=" + name + ", dialyWage=" + dialyWage + "]";
}
/**
 * @return the eno
 */
public int getEno() {
	return eno;
}
/**
 * @param eno the eno to set
 */
public void setEno(int eno) {
	this.eno = eno;
}
/**
 * @return the name
 */
public String getName() {
	return name;
}
/**
 * @param name the name to set
 */
public void setName(String name) {
	this.name = name;
}
/**
 * @return the dialyWage
 */
public double getDialyWage() {
	return dialyWage;
}
/**
 * @param dialyWage the dialyWage to set
 */
public void setDialyWage(double dialyWage) {
	this.dialyWage = dialyWage;
}
public Employee(Integer eno, String name, Double dialyWage) {
	super();
	this.eno = eno;
	this.name = name;
	this.dialyWage = dialyWage;
}


}
