package compositeExample;

import java.util.function.BiFunction;

public class StudentCreatorBiFunction {
	
	public static void main(String[] args) {
		BiFunction<String, Integer, Student> create = (name,roll)->new Student(name, roll);
		
		Student s = create.apply("Aniket", 500);
		System.out.println("created result : "+s);
	}

}
