package supplier;

import java.util.Random;
import java.util.function.BiFunction;
import java.util.function.Supplier;

public class SupplierExample {
	
	public static void main(String[] args) {
		
		Supplier<String> nameRandom = ()->{
			String arr[] = {"Aniket","Amit","Dibyendu","Legacy"};
			int position = new Random().nextInt(3);
			return arr[position];
		};
		
		
		System.out.println("---Random name -----"+nameRandom.get());
	
	//generate 6 digit OTP
		Supplier<String> OTPGenerator = ()->{
			String otp = "";
			for(int k = 0 ; k<6;k++)
			{
				int position = new Random().nextInt(9);
				otp = otp+position;
			}
			return otp;
		};
		
		System.out.println("---Random OTP -----"+OTPGenerator.get());
	
	// GENERATE RANDOM PASSWORD 
		//0-9 , A-Z, #,@,$ - 10 CHAR LONG
		
		//SME WAY WE CAN DO 
		
	}

}
