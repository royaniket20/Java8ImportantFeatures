package supplier;

import java.util.function.BiFunction;

public class BiFunctionExample {

	public static void main(String[] args) {
		BiFunction<Integer, Integer,Integer> biPredicate = (x,y)->{
			if(x%2==0 && x>y)
				return x+y;
			else
				return x-y;
				
		};
		
		
		System.out.println("Check bipredicate : - "+biPredicate.apply(6, 3));
	}
}
