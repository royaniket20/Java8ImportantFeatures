package consumers;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class ConsumerExample {

	public static void main(String[] args) {
		List<Movie> movies = new ArrayList<Movie>();
		
		movies.add(new Movie("AAA", "BBB", "CCC"));
		
		
		Consumer<Movie> moviePrinting = movie1 -> System.out.println(movie1);
		
		movies.forEach(m->moviePrinting.accept(m));

	}

}
