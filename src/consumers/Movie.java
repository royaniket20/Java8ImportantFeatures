package consumers;

public class Movie {
private String hero;
private String heroin;
private String name;
public Movie(String hero, String heroin, String name) {
	super();
	this.hero = hero;
	this.heroin = heroin;
	this.name = name;
}
public String getHero() {
	return hero;
}
public void setHero(String hero) {
	this.hero = hero;
}
public String getHeroin() {
	return heroin;
}
public void setHeroin(String heroin) {
	this.heroin = heroin;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
@Override
public String toString() {
	return "Movie [hero=" + hero + ", heroin=" + heroin + ", name=" + name + "]";
}


}
